#!/bin/bash

# This script unpacks the 16GB image, and must be run with admin privileges

IMG_FILE=$1

if [ ! -f "$IMG_FILE" ]; then
  echo "$IMG_FILE" not found
  exit 1
fi

OUTPUT_FOLDER="output/`basename $1`_`date +"%Y%m%d_%H%M%S"`"
echo "Output folder is $OUTPUT_FOLDER"

gzipped=0

if [ "${IMG_FILE: -3}" == ".gz" ]; then
  echo "Gunzipping the image"
  gzipped=1
  gunzip -v "$IMG_FILE"
  IMG_FILE=`echo "$IMG_FILE" | sed 's/\.gz//'`
fi

mkdir -p "$OUTPUT_FOLDER/mount"

bs=`fdisk -l "$IMG_FILE"| grep "Units:" | cut -d" " -f8`
echo "Image file has a block size of $bs bytes"

for p in  $(seq 1 4); do
  offset=`fdisk -l "$IMG_FILE" | grep "$IMG_FILE"$p | sed 's/ \{1,\}/|/g' | cut -d"|" -f2`
  echo "Mounting partition $p (starts at offset $offset)"
  mount -o loop,offset=$[$bs*$offset] "$IMG_FILE" "$OUTPUT_FOLDER/mount"
  mkdir "$OUTPUT_FOLDER/p$p"
  cp -rfpv "$OUTPUT_FOLDER/mount/*" "$OUTPUT_FOLDER/p$p"
  umount "$OUTPUT_FOLDER/mount"
done

if [ $gzipped -eq 1 ]; then
  echo "Gzipping back the image"
  gzip "$IMG_FILE"
fi

rm -rf "$OUTPUT_FOLDER/mount"

exit 0

# Retro Arcade Mini OFW Mod

```
fdisk -l rs07-ofw13-mod.img
Disk rs07-ofw13-mod.img: 14.6 GiB, 15640559616 bytes, 30547968 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device              Boot  Start      End  Sectors  Size Id Type
rs07-ofw13-mod.img1       16384   733183   716800  350M 83 Linux
rs07-ofw13-mod.img2      733184   937983   204800  100M 83 Linux
rs07-ofw13-mod.img3      937984   999423    61440   30M 83 Linux
rs07-ofw13-mod.img4      999424 30392319 29392896   14G  b W95 FAT32
```
## First partition

Mount points (part of it at least) set in `/etc/inittab`.

## Second partition

`sudo mount -o loop,offset=$[512*733184] rs07-ofw13-mod.img img/`

Dmenu resources are located in `/local/dmenu`. It's possible to add wallpapers,
but free space is very limited (need to repack image). Already added a couple.

`local/dmenu/dmenu.ini` what about the one in 3rd partition?

The emulators executables and resources are also located here. Interestingly
there is an SMS emulator in `/game/sms`, so hopefully if we add roms in an `sms`
folder this could work?

`/game/resources` contains the start+select menu background. [NOPE - hard-coded in emu binaries]

## Third partition

`sudo mount -o loop,offset=$[512*937984] rs07-ofw13-mod.img img/`

`sudo nano img/dmenu/dmenu.ini`

This is the dmenu launcher config file, there is a sound volume option set to
100, so hopefully lowering it to 1 like in gameblabla's CFW would allow to fix
the crazy loud speaker.

### Permissions

Modded image:

```
nicolas@Kirin ~/dev/git/retromini/retro-arcade-mini-ofw-mod/mount/dmenu $ ll
total 5.0K
drwxr-xr-x  2 nicolas nicolas 1.0K Oct  1  2010 .
drwxr-xr-x 22 root    root    1.0K Nov 14 13:42 ..
-rw-r--r--  1 root    root       4 Oct  1  2010 crc_tmp.ini
-rwxr-xr-x  1 root    root     464 Oct  1  2010 dmenu.ini
-rwxr--r--  1 nicolas nicolas   17 Oct  1  2010 dmenu_lock.ini
```

Original:

```
nicolas@Kirin ~/Downloads/Gaming/retrogaming/rs-07/dumps $ sudo ls -l /home/nicolas/dev/git/retromini/retro-arcade-mini-ofw-mod/mount/dmenu/
total 3
-rw-r--r-- 1 root    root      4 Oct  1  2010 crc_tmp.ini
-rw-r--r-- 1 root    root    446 Oct  1  2010 dmenu.ini
-rwxr--r-- 1 nicolas nicolas  17 Oct  1  2010 dmenu_lock.ini
```

These different permissions on `dmenu.ini` may cause settings change not to be saved.

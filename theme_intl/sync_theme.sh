#!/bin/sh

PREFIX="../sd/p2/local/dmenu/themes/ipen"
TS=`date +"%Y%m%d_%H%M%S"`

for l in `ls locale`; do
  echo "Generating theme files for language $l"
  TMP_SH="sed_"$l"_"$TS".sh"
  if [ 'chn' = $l ]; then
    cfg="$PREFIX"/theme.cfg
    coolcfg="$PREFIX"/cooltheme.cfg
  else
    cfg="$PREFIX"_"$l"/theme.cfg
    coolcfg="$PREFIX"_"$l"/cooltheme.cfg
  fi

  cp -vf theme.cfg $cfg
  cat locale/$l/theme.txt | while read s; do
    echo "sed -i \"s/%`echo $s | cut -d= -f1`%/`echo $s | cut -d= -f2`/g\" $cfg" >> $TMP_SH
  done;
  chmod +x $TMP_SH
  ./$TMP_SH
  rm $TMP_SH
  chmod 764 $cfg

  cp -vf cooltheme.cfg $coolcfg
  cat locale/$l/cooltheme.txt | while read s; do
    echo "sed -i \"s/%`echo $s | cut -d= -f1`%/`echo $s | cut -d= -f2`/g\" $coolcfg" >> $TMP_SH
  done;
  chmod +x $TMP_SH
  ./$TMP_SH
  rm $TMP_SH
  chmod 764 $coolcfg
done

#!/bin/bash

# This script repacks the changes into the base image, and must be run with admin privileges

BASE_IMG=rs07-stock-fw-v1.3-base.img.gz

TS="`date +"%Y%m%d_%H%M%S"`"
IMG="output/rs07-ofw13-mod-$TS.img"
echo "Output image is $IMG.gz"
cp -vf "$BASE_IMG" "$IMG".gz

MOUNT="mount$TS"
mkdir -p $MOUNT

echo "Gunzipping the image"
gunzip -v "$IMG".gz

bs=`fdisk -l "$IMG"| grep "Units:" | cut -d" " -f8`
echo "Image file has a block size of $bs bytes"

p=2
offset=`fdisk -l "$IMG" | grep "$IMG"$p | sed 's/ \{1,\}/|/g' | cut -d"|" -f2`
echo "Mounting partition $p (starts at offset $offset)"
mount -o loop,offset=$[$bs*$offset] "$IMG" "$MOUNT"
cp -rfpv sd/p$p/* "$MOUNT"
umount "$MOUNT"

echo "Gzipping the image"
gzip "$IMG"

rm -rf "$MOUNT"

exit 0
